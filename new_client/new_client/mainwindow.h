#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QTcpSocket* socket;
    QByteArray Data;
private:
    Ui::MainWindow *ui;
    QString login;
public slots:
    void sockReady();
    void sockDisc();
    QByteArray QString_to_QByteArray(QString templ);
    QString QByteArray_to_QString(QByteArray templ);
private slots:

    void on_login_2_clicked(bool checked);
    void on_registration_clicked(bool checked);
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
