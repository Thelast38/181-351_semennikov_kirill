#ifndef REGISTER_H
#define REGISTER_H

#include <QWidget>
#include <QTcpSocket>
namespace Ui {
class Register;
}

class Register : public QWidget
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = nullptr);
    ~Register();
    QTcpSocket* socket;
    bool check_number(QString number);
    bool check_password(QString password);
private slots:
    void on_pushButton_choose_clicked();

    void on_lineEdit_number_editingFinished();

    void on_lineEdit_password_editingFinished();

    void on_lineEdit_login_editingFinished();

    void on_pushButton_register_clicked();

private:
    Ui::Register *ui;
signals:
    void regist(QByteArray img);
};

#endif // REGISTER_H
