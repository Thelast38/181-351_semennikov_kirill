#ifndef ADMIN_PAGE_H
#define ADMIN_PAGE_H

#include <QWidget>

namespace Ui {
class Admin_page;
}

class Admin_page : public QWidget
{
    Q_OBJECT

public:
    explicit Admin_page(QWidget *parent = nullptr);
    Admin_page(QString a);
    ~Admin_page();
public slots:
    void set_data(QString a,QString b,QString cc);
signals:
    void Delete(QString login);
private slots:
    void on_pushButton_clicked();

    void on_comboBox_2_activated(const QString &arg1);

    void on_comboBox_activated(const QString &arg1);

    void set_data_2(QString arg);
private:
    QList <QString> login;//full
    QList <QString> pos;
    QList <QString> pay;
    QString costul;
    QString costul1;
    Ui::Admin_page *ui;
};

#endif // ADMIN_PAGE_H
