#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "Crypter.h"
#include <QCryptographicHash>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_login_clicked()
{
    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    qDebug()<<"helo";
    socket->connectToHost("127.0.0.1",6000);
    QString buffer="10$";
    buffer+= this->ui->lineEdit_login->text()+"$";
    QCryptographicHash b(QCryptographicHash::Md4);
    b.addData(QString_to_QByteArray(this->ui->lineEdit_password->text()));
    QString costul = QString::fromStdString(b.result().toHex().toStdString());
    QCharRef costul2=costul[0];//Потому что 1 костыля мало!!!!
    login1 = this->ui->lineEdit_login->text();
    costul[0]=costul[1];
    costul[1]=costul2;
    buffer+=costul;

    socket->write(QString_to_QByteArray(Crypter::cryptString(buffer)));
}

void MainWindow::on_pushButton_register_clicked()
{
reg = new Register();
reg->show();
//connect(this,SLOT(Registe()),reg,SIGNAL(Registe()));

}

QByteArray MainWindow::QString_to_QByteArray(QString templ)
{
    std::string String_buffer=templ.toStdString();
    QByteArray QByteArray_buffer;
    const char *cstr_buffer = String_buffer.c_str();
    QByteArray_buffer.append(cstr_buffer);
    return QByteArray_buffer;
}

QString MainWindow::QByteArray_to_QString(QByteArray templ)
{
    const char *cstr_buffer=templ.data();
    QString str = QString::fromUtf8(cstr_buffer);
    return str;
}

void MainWindow::Registe(QByteArray img)
{
}

void MainWindow::Delete(QString login)
{
    socket->write(QString_to_QByteArray(Crypter::cryptString("30$"+login)));
}

void MainWindow::Change()
{
    socket->write(QString_to_QByteArray(Crypter::cryptString("40$"+login1)));
}
void MainWindow::sockReady()
{
    if (socket->waitForConnected(100))
    {
        socket->waitForReadyRead(100);
        Data = socket->readAll();
        qDebug()<<Data;
        if(Data[0]=='1' && Data[1]=='1' && Data[2]=='u'){
            usr = new User_page();
            usr->show();
            connect(usr,&User_page::change,this,&MainWindow::Change);
        }
        else if (Data[0]=='1' && Data[1]=='1' && Data[2]=='a') {
            Data.remove(0,4);
            adm = new Admin_page();
            connect(this,&MainWindow::admin,adm,&Admin_page::set_data);
            connect(adm,&Admin_page::Delete,this,&MainWindow::Delete);
            socket->write(QString_to_QByteArray(Crypter::cryptString("50$")));
            bones = Data;

        }
        else if (Data[0]=='5' && Data[1]=='0') {
            Data.remove(0,3);
            bones1=Data;
            //emit admin(bones,Data);
            socket->write(QString_to_QByteArray(Crypter::cryptString("60$")));
             adm->show();
        }
        else if (Data[0]=='6' && Data[1]=='0') {
            Data.remove(0,3);

            emit admin(bones,bones1,Data);
            //socket->write(QString_to_QByteArray(Crypter::cryptString("60$")));
            // adm->show();
        }
    }
}
void MainWindow::sockDisc()
{
    socket->deleteLater();
}
