#include "register.h"
#include "ui_register.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QDebug>
#include <Crypter.h>
#include <QCryptographicHash>
Register::Register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
}
QByteArray QString_to_QByteArray(QString templ)
{
    std::string String_buffer=templ.toStdString();
    QByteArray QByteArray_buffer;
    const char *cstr_buffer = String_buffer.c_str();
    QByteArray_buffer.append(cstr_buffer);
    return QByteArray_buffer;
}
Register::~Register()
{
    delete ui;
}

bool Register::check_number(QString number)
{
    if(!(number.length()==8 || number.length()==9)){
        return false;
    }
    else if(!(number[2] >='0' && number[2] <='9')&& !(number[1] >='0' && number[1] <='9')&& !(number[3] >='0' && number[3] <='9')&& !(number[6] >='0' && number[6] <='9')&& !(number[7] >='0' && number[7] <='9')){
        return false;
    }
    else if ( number.length()==9) {
        if(!(number[8] >='0' && number[8] <='9')){
            return false;
        }
    }
    else if (!(number[0] >='A' && number[0] <='Z')&& !(number[4] >='A' && number[4] <='Z')&& !(number[5] >='A' && number[5] <='Z')) {
        return false;
    }
    else {
        return true;
    }

}

bool Register::check_password(QString password)
{
    if(password.length()<8){
        return false;
    }
    for (int i = 0;i< password.length();i++) {
        if(!((password[i]>='0' && password[i]<='9') || (password[i]>='a' && password[i]<='z') || (password[i]>='A' && password[i]<='Z'))){
            return false;
        }
    }
    return true;
}

void Register::on_pushButton_choose_clicked()
{

}
//a123bc333
void Register::on_lineEdit_number_editingFinished()
{
    QString line(this->ui->lineEdit_number->text());
    if(check_number(line)){
        this->ui->label_number->setStyleSheet("color: green");
    }
    else {
        this->ui->label_number->setStyleSheet("color: red");
    }

}

void Register::on_lineEdit_password_editingFinished()
{
    QString line(this->ui->lineEdit_password->text());
    if(check_password(line)){
        this->ui->label_password->setStyleSheet("color: green");
    }
    else {
        this->ui->label_password->setStyleSheet("color: red");
    }
}

void Register::on_lineEdit_login_editingFinished()
{
    QString line(this->ui->lineEdit_login->text());
    if(check_password(line)){
        this->ui->label_login->setStyleSheet("color: green");
    }
    else {
        this->ui->label_login->setStyleSheet("color: red");
    }
}

void Register::on_pushButton_register_clicked()
{
    if(check_number(ui->lineEdit_number->text()) && check_password(ui->lineEdit_password->text()) && check_password(ui->lineEdit_login->text())){
        socket = new QTcpSocket(this);
        connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
        connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
        qDebug()<<"helo";
        socket->connectToHost("127.0.0.1",6000);
        QString buffer="20$";
        buffer+= this->ui->lineEdit_login->text()+"$";
        //qDebug()<<buffer;
        QCryptographicHash b(QCryptographicHash::Md4);
        b.addData(QString_to_QByteArray(this->ui->lineEdit_password->text()));
        QString costul = QString::fromStdString(b.result().toHex().toStdString());
        QCharRef costul2=costul[0];//Потому что 1 костыля мало!!!!

        costul[0]=costul[1];
        costul[1]=costul2;

        buffer+=costul+"$";
        qDebug()<<buffer;
         buffer+= this->ui->lineEdit_number->text();
        //qDebug()<<QString_to_QByteArray(Crypter::cryptString(buffer));
        socket->write(QString_to_QByteArray(Crypter::cryptString(buffer)));
    }
}
