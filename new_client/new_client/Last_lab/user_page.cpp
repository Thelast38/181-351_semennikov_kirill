#include "user_page.h"
#include "ui_user_page.h"

User_page::User_page(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::User_page)
{
    ui->setupUi(this);
   // QPixmap pixmap("qrc:/new/img/Garage.png");
    QPixmap pixmap1(":/new/img/Garage.png");

    ui->label->setPixmap(pixmap1);

    //ui->label->setMask(pixmap.mask());
    QPixmap pixmap(":/new/img/car.png");
    QIcon ButtonIcon(pixmap);
    ui->pushButton->setIcon(ButtonIcon);
    ui->pushButton->setIconSize(ui->pushButton->size());
}

User_page::~User_page()
{
    delete ui;
}

void User_page::on_pushButton_clicked()
{
    emit change();
}
