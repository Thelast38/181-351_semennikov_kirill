#include <QCoreApplication>
#include <iostream>
#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // сами криптогрфические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка
#include <openssl/aes.h>
#include <fstream>
#include<QDebug>
using namespace std;

#pragma comment (lib, "ws2_32.LIB")
#pragma comment (lib, "gdi32.LIB")
#pragma comment (lib, "advapi32.LIB")
#pragma comment (lib, "crypt32")
#pragma comment (lib, "user32")
#pragma comment (lib, "wldap32")
static int writer(char *data,
    size_t size,
    size_t nmemb,
    std::string *writerData)
{
    if (writerData == NULL)
        return 0;

    writerData->append(data, size*nmemb);

    return size * nmemb;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv)
            ;
     qDebug() << 'a';
    struct name_of_my_struct // ñðîäíè êëàññó
        {
            name_of_my_struct()
            {

            }

            int a;
            double b;
            int fnc1()
            {
                return a;
            }
        };
        unsigned char *plaintext =
            (unsigned char *)"dsdssddffdadfafddfas";// èñõîäíûé òåêñò
        int plaintext_len = strlen((char *)plaintext); // äëèíà òåêñòà
        unsigned char *key = (unsigned char *)"0123456789"; // ïàðîëü (êëþ÷)
        unsigned char *iv = (unsigned char *)"0123456789012345"; // èíèöèàëèçèðóþùèé âåêòîð, ðàíäîìàéçåð
        unsigned char cryptedtext[256]; // çàøèôðîâàííûé ðåçóëüòàò
        unsigned char decryptedtext[256]; // ðàñøèôðîâàííûé ðåçóëüòàò

    qDebug() << 'a';

            // 1. Ñîçäà¸òñÿ óêàçàòåëü íà íåñóùåñòâóþùóþ ñòðóêòóðó
            // ñòðóêòóðà - òèï äàííûõ â C++, áëèçêà ê ÊËÀÑÑÓ, ðàçëè÷èÿ ìèíèìàëüíû
            EVP_CIPHER_CTX *ctx; // structure

            // 2. Äëÿ óêàçàòåëÿ ñîçäà¸òñÿ ïóñòàÿ ñòðóêòóðà íàñòðîåê (ìåòîä, êëþ÷, âåêòîð èíèöèàëèçàöèè è ò.ä.)
            ctx = EVP_CIPHER_CTX_new(); // ñîçäàíèå ñòðóêòóðû ñ íàñòðîéêàìè ìåòîäà

            // 3. Ñòðóêòóðà EVP_CIPHER_CTX çàïîëíÿåòñÿ íàñòðîéêàìè
            EVP_EncryptInit_ex(ctx, // ññûëêà íà îáúåêò/ñòðóêòóðó, êóäà çàíîñÿòñÿ ïàðàìåòðû
                EVP_aes_256_cbc(), // ññûëêà íà øèôðóþùåå ÿäðî AES 256 (ôóíêöèþ ñ àëãîðèòìîì)
                NULL,
                key, // êëþ÷/ïàðîëü/ñåêðåò
                iv); // ðàíäîìàéçåð (ñëó÷àéíûé íà÷àëüíûé âåêòîð)

            // 4. ÑÀÌ ÏÐÎÖÅÑÑ ØÈÔÐÎÂÀÍÈß - ÔÓÊÍÖÈß EVP_EncryptUpdate
            int len;
            EVP_EncryptUpdate(ctx, // îáúåêò ñ íàñòðîéêàìè
                cryptedtext, // âõîäíîé ïàðàìåòð: ññûëêà, êóäà ïîìåùàòü çàøèôðîâàííûå äàííûå
                &len, // âõîäíîé ïàðàìåòð: äëèíà ïîëó÷åííîãî øèôðà
                plaintext, // âõîäíîé ïàðàìåòð: ÷òî øèôðîâàòü
                plaintext_len); // âõîäíîé ïàðàìåòð : äëèíà âõîäíûõ äàííûõ
            int cryptedtext_len = len;

            // 5. Ôèíàëèçàöèÿ ïðîöåññà øèôðîâàíèÿ
            // íåîáõîäèìà, åñëè ïîñëåäíèé áëîê çàïîëíåí äàííûìè íå ïîëíîñòüþ
            EVP_EncryptFinal_ex(ctx, cryptedtext + len, &len);
            cryptedtext_len += len;

            // 6. Óäàëåíèå ñòðóêòóðû
            EVP_CIPHER_CTX_free(ctx);
            // âûâîä çàøèôðîâàííûõ äàííûõ
            for (int i = 0; i < cryptedtext_len; i++)
            {
                qDebug() << hex << cryptedtext[i];
                if ((i + 1) % 32 == 0) cout << endl;
            }
             qDebug() << endl;

            // ÐÀÑØÈÔÐÎÂÊÀ
            // 1.
            ctx = EVP_CIPHER_CTX_new(); // ñîçäàíèå ñòðóêòóðû ñ íàñòðîéêàìè ìåòîäà

            // 2.
            EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // èíèöèàëèçàöèÿ ìåòîäîì AES, êëþ÷îì è âåêòîðîì

            // 3.
            EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, cryptedtext_len);  // ÑÎÁÑÒÂÅÍÍÎ, ØÈÔÐÎÂÀÍÈÅ

            // 4.
            int dectypted_len = len;
            EVP_DecryptFinal_ex(ctx, decryptedtext + len, &len);

            // 5.
            dectypted_len += len;
            EVP_CIPHER_CTX_free(ctx);
            decryptedtext[dectypted_len] = '\0';
             qDebug() << decryptedtext << endl;

       return a.exec();

}
