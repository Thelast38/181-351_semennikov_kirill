#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
#include<register.h>
#include<admin__main_page.h>
#include "user_main_page.h"
#include"personal_account.h"
#include "list_of_peple.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTcpSocket* socket;
    QByteArray Data;

public slots:

    void sockReady();
    void sockDisc();
    QByteArray QString_to_QByteArray(QString templ);
    QString QByteArray_to_QString(QByteArray templ);
    void close_register();
    void new_regiser(QString login,QString password,QString passport,QString numberCar,QString modelCar);
    void anser_to_reg(bool code);
    void open_admin();
    void close_admin();
    void open_user(int code);//0 - out 1-in
    void close_user();
    void open_personal_account();
    void change_car_status();
    void add_data_to_personal_account(QString code);
    void close_personal_accont();
    void open_list_peple_fromaAdmin();
    void set_list_people_info(QString Data,int collom);
private slots:
    void on_pushButton_clicked();

    void on_add1_clicked();

    void on_registration_clicked();
signals:
    void personal_account(QString login,QString number_passport,QString number_car,QString model_car);
private:
    List_of_peple *list_people;
    QString login;
    Ui::MainWindow *ui;
    Register *Register_w;
    admin__Main_page *admin_main;
    User_main_page *user_main;
    Personal_account *personal_acount;
};

#endif // MAINWINDOW_H
