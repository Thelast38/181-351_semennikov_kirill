#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<string>
#include <QCryptographicHash>
#include <QMessageBox>
#include<QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->password->setEchoMode(QLineEdit::Password);
    socket = new QTcpSocket(this);

        connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
        connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));

        socket->connectToHost("127.0.0.1",6000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open_list_peple_fromaAdmin()
{
    admin_main->hide();
    socket->write("22$");
   list_people= new List_of_peple();
   //list_people->show();
}

void MainWindow::set_list_people_info(QString Data, int collom)
{
    Data.remove(0,3);
    list_people->set_Data(Data);
    list_people->set_Table_row(list_people->count_rows(Data));
    list_people->set_Table_Data(collom);
    if(collom==4){
        list_people->show();
    }
}

void MainWindow::on_pushButton_clicked()
{


}

void MainWindow::sockDisc()
{
    socket->deleteLater();
}

QByteArray MainWindow::QString_to_QByteArray(QString templ)
{
    std::string String_buffer=templ.toStdString();
    QByteArray QByteArray_buffer;
    const char *cstr_buffer = String_buffer.c_str();
    QByteArray_buffer.append(cstr_buffer);
    return QByteArray_buffer;
}

QString MainWindow::QByteArray_to_QString(QByteArray templ)
{
    const char *cstr_buffer=templ.data();
    QString str = QString::fromUtf8(cstr_buffer);
    return str;
}

void MainWindow::close_register()
{
    Register_w->close();
    this->show();
}

void MainWindow::new_regiser(QString login,QString password,QString passport,QString numberCar,QString modelCar)
{
QString buffer;
buffer+="11$";
buffer+="login=$";
buffer+=login+"$,";
QCryptographicHash b(QCryptographicHash::Md4);
b.addData(password.toStdString().c_str(),password.length());
QString bufffer=b.result().toHex().toStdString().c_str();
buffer+="password=$";
buffer+=bufffer+"$,";
buffer+="passport=$";
buffer+=passport+"$,";
buffer+="numberCar=$";
buffer+=numberCar+"$,";
buffer+="modelCar=$";
buffer+=modelCar+"$}";
socket->write(QString_to_QByteArray(buffer));
}

void MainWindow::anser_to_reg(bool code)
{
    if(code){
        QMessageBox msgBox;
        msgBox.setText("Вы зарегистрировались");
        msgBox.exec();
        Register_w->close();
        this->show();
    }
    else {
        QMessageBox msgBox;
        msgBox.setText("Ваш логин занят");
        msgBox.exec();
    }
}

void MainWindow::open_admin()
{
this->hide();

admin_main=new admin__Main_page();
connect(admin_main,SIGNAL(open_list_of_people()),this,SLOT(open_list_peple_fromaAdmin()));
connect(admin_main,SIGNAL(close_windows()),this,SLOT(close_admin()));
admin_main->show();
}

void MainWindow::close_admin()
{
    admin_main->close();
    this->show();
}

void MainWindow::open_user(int code)
{
    this->hide();
    user_main=new User_main_page();
    user_main->set_first_button(code);
    connect(user_main,SIGNAL(close_windows()),this,SLOT(close_user()));
    connect(user_main,SIGNAL(account_user()),this,SLOT(open_personal_account()));
    connect(user_main,SIGNAL(change_status_avto()),this,SLOT(change_car_status()));
    user_main->show();
}

void MainWindow::close_user()
{
    user_main->close();
    this->show();
}

void MainWindow::open_personal_account()
{
    user_main->hide();
    QString buffer="20$"+login+"$";
    socket->write(QString_to_QByteArray(buffer));
   /* personal_acount = new Personal_account();
    personal_acount->show();*/
}

void MainWindow::change_car_status()
{
    QString buffer="21$"+login+"$";
    socket->write(QString_to_QByteArray(buffer));
}

void MainWindow::add_data_to_personal_account(QString code)
{
    QString passport;
    QString number_car;
    QString model_car;
    int i=4;
    for (i;i<code.length();i++) {
        if(code[i]=="$"){
            break;
        }
        else {
            passport+=code[i];
        }
    }
    i++;
    qDebug()<<passport;
    for (i;i<code.length();i++) {
        if(code[i]=="$"){
            break;
        }
        else {
            number_car+=code[i];
        }
    }
    i++;
    qDebug()<<number_car;
    for (i;i<code.length();i++) {
        if(code[i]=="$"){
            break;
        }
        else {
            model_car+=code[i];
        }
    }
    qDebug()<<model_car;
    personal_acount = new Personal_account();
    connect(personal_acount,SIGNAL(close_windows()),this,SLOT(close_personal_accont()));
    personal_acount->set_data(login,passport,number_car,model_car);
    personal_acount->show();
}

void MainWindow::close_personal_accont()
{
    personal_acount->close();
    user_main->show();
}

void MainWindow::sockReady()
{
    if (socket->waitForConnected(100))
    {
        socket->waitForReadyRead(100);
        Data = socket->readAll();
        qDebug()<<Data;
        QString buffer=QByteArray_to_QString(Data);
        std::string buf=buffer.toStdString();
        buf=buf.substr(0,3);
        if(Data=="111"){
            anser_to_reg(true);
        }
        else if (Data=="011") {
             anser_to_reg(false);
        }
        else if (Data=="110a") {
             open_admin();
        }
        else if (Data=="110u0") {
             open_user(0);
        }
        else if (Data=="110u1") {
             open_user(1);
        }
        else if (buf=="022") {
             set_list_people_info(buffer,0);
        }
        else if (buf=="122") {
             set_list_people_info(buffer,1);
        }
        else if (buf=="222") {
             set_list_people_info(buffer,2);
        }
        else if (buf=="322") {
             set_list_people_info(buffer,3);
        }
        else if (buf=="422") {
             set_list_people_info(buffer,4);
        }

    }
}

void MainWindow::on_add1_clicked()
{
    QString buffer="10$login=$";
    buffer+= this->ui->login->text();
    login = this->ui->login->text();
    QCryptographicHash b(QCryptographicHash::Md4);
    QString c=this->ui->password->text();
    b.addData(c.toStdString().c_str(),c.length());
    c.fromStdString(b.result().toHex().toStdString());
    buffer+="$,password=$";
    QString bufffer=b.result().toHex().toStdString().c_str();
    buffer+=bufffer;
    buffer+="$";
    Data.clear();
   Data=QString_to_QByteArray(buffer);
   this->ui->login->clear();
   this->ui->password->clear();
    socket->write(Data);
}

void MainWindow::on_registration_clicked()
{

    this->hide();
    Register_w =new Register();
    connect(Register_w,&Register::new_Register,this,&MainWindow::new_regiser);
    connect(Register_w,SIGNAL(close_windows()),this,SLOT(close_register()));
    Register_w->show();
}
