#include "personal_account.h"
#include "ui_personal_account.h"

void Personal_account::set_data(QString login, QString number_passport, QString number_car, QString model_car)
{
    this->ui->label_login_out->setText(login);
    this->ui->label_pasport_out->setText(number_passport);
    this->ui->label_number_car_out->setText(number_car);
    this->ui->label_mark_car_out->setText(model_car);
}

Personal_account::Personal_account(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Personal_account)
{
    ui->setupUi(this);
}

Personal_account::~Personal_account()
{
    delete ui;
}

void Personal_account::on_back_button_clicked()
{
    emit close_windows();
}

void Personal_account::on_change_button_clicked()
{

}
