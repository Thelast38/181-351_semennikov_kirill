#ifndef USER_MAIN_PAGE_H
#define USER_MAIN_PAGE_H

#include <QWidget>

namespace Ui {
class User_main_page;
}

class User_main_page : public QWidget
{
    Q_OBJECT

public:
    explicit User_main_page(QWidget *parent = nullptr);
    ~User_main_page();
    void set_first_button(int code);//1-in 0-out
signals:
    void close_windows();
    void account_user();
    void change_status_avto();
private slots:


    void on_back_button_clicked();

    void on_account_button_clicked();

    void on_status_button_clicked();

private:
    Ui::User_main_page *ui;
};

#endif // USER_MAIN_PAGE_H
