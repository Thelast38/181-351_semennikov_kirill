#ifndef LIST_OF_PEPLE_H
#define LIST_OF_PEPLE_H

#include <QWidget>

namespace Ui {
class List_of_peple;
}

class List_of_peple : public QWidget
{
    Q_OBJECT

public:
    int count_rows(QString data);
    explicit List_of_peple(QWidget *parent = nullptr);
     void set_Table_row(int rows);
     void set_Table_Data(int collums);
     void set_Data(QString data);
     void clear_Data();
    ~List_of_peple();

private:
     int rowss;
     QString *Data;
    Ui::List_of_peple *ui;
};

#endif // LIST_OF_PEPLE_H
