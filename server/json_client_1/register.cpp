#include "register.h"
#include "ui_register.h"
#include <string>
#include <QMessageBox>
Register::Register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
}

bool Register::check_password(QString password)
{
    if(password.length()<9 || password.length()>20){
        return false;
    }
    std::string password_bufer=password.toStdString();
    for (int i=0;i<password.length();i++) {
        if((password_bufer[i]>='a' && password_bufer[i]<='z')||(password_bufer[i]>='0' && password_bufer[i]<='9') ||(password_bufer[i]>='A' && password_bufer[i]<='Z')){

        }
        else {
            return false;
        }
    }
    return true;
}

bool Register::check_passport(QString passport)
{
    std::string password_bufer=passport.toStdString();
    if (passport.length()!=10) {
        return false;
    }
    for (int i=0;i!=10;i++) {
        if((password_bufer[i]>='0' && password_bufer[i]<='9')){

    }
        else {
            return false;
        }
    }
    return true;
}

bool Register::check_login(QString login)
{
    if(login.length()<6){
        return false;
    }
    if(login.length()>16){
        return false;
    }
    std::string password_bufer=login.toStdString();
    for (int i=0;i<login.length();i++) {
        if((password_bufer[i]>='a' && password_bufer[i]<='z')||(password_bufer[i]>='0' && password_bufer[i]<='9') ||(password_bufer[i]>='A' && password_bufer[i]<='Z')){

        }
        else {
            return false;
        }
    }
    return true;
}

bool Register::check_numberCar(QString numberCar)
{
    std::string password_bufer=numberCar.toStdString();
    if (numberCar.length()!=3) {
        return false;
    }
    for (int i=0;i!=3;i++) {
        if((password_bufer[i]>='0' && password_bufer[i]<='9')){

    }
        else {
            return false;
        }
    }
    return true;
}

bool Register::check_numberCar1(QString numberCar)
{
    std::string password_bufer=numberCar.toStdString();
    if (numberCar.length()>3 || numberCar.length()<2) {
        return false;
    }
    for (int i=0;i<numberCar.length();i++) {
        if((password_bufer[i]>='0' && password_bufer[i]<='9')){

    }
        else {
            return false;
        }
    }
    return true;
}


Register::~Register()
{
    delete ui;
}

void Register::on_push_back_clicked()
{
    emit close_windows();
}

void Register::on_push_register_clicked()
{
    QString Buffer="";
    if(!check_login(this->ui->login_out->text())){
        Buffer+="<p>Логин должен содержать от 5 до 15 символов</p><p>Логин должен содержать только английские буквы и цифры</p>";
        this->ui->login_label->setStyleSheet("color:red");
    }
    else {
        this->ui->login_label->setStyleSheet("color:black");
    }
    if(!check_password(this->ui->password_out->text())){
        Buffer+="<p>Пароль должен содержать от 8 до 20 символов</p><p>Пароль должен содержать только английские буквы и цифры</p>";
        this->ui->password_label->setStyleSheet("color:red");
    }
    else {
        this->ui->password_label->setStyleSheet("color:black");
    }
    if(!check_passport(this->ui->passport_out->text())){
        Buffer+="<p>Паспорт должен содержать 10 символов</p>";
        this->ui->passport_label->setStyleSheet("color:red");
    }
    else {
        this->ui->passport_label->setStyleSheet("color:black");
    }
    if(!check_numberCar(this->ui->number1_car_out->text())){
        Buffer+="<p>Номер автомобиля должен содержать 3 цифры</p>";
        this->ui->number_labe->setStyleSheet("color:red");
    }
    else {
        this->ui->number_labe->setStyleSheet("color:black");
    }
    if(!check_numberCar1(this->ui->number2_car_out->text())){
        Buffer+="<p>Регион автомобиля должен содержать 2-3 цифры</p>";
        this->ui->number_labe->setStyleSheet("color:red");
    }
    else {
        this->ui->number_labe->setStyleSheet("color:black");
    }
    if(Buffer==""){
        QString buffer_number;
        buffer_number+=this->ui->first_word_number_out->currentText();
        buffer_number+=this->ui->number1_car_out->text();
        buffer_number+=this->ui->second_word_number_out->currentText();
        buffer_number+=this->ui->tree_word_number_out->currentText();
        buffer_number+=this->ui->number2_car_out->text();
        emit new_Register(this->ui->login_out->text(),this->ui->password_out->text(),this->ui->passport_out->text(),buffer_number,this->ui->model_out->currentText());
    }
    else {
        QMessageBox msgBox;
                    msgBox.setText(Buffer);
                    msgBox.exec();
    }
}
