#include "user_main_page.h"
#include "ui_user_main_page.h"

User_main_page::User_main_page(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::User_main_page)
{
    ui->setupUi(this);
}

User_main_page::~User_main_page()
{
    delete ui;
}

void User_main_page::set_first_button(int code)
{
    if(code==0){
    this->ui->status_button->setText("Заехать в гараж");}
    else {
        this->ui->status_button->setText("Выехать из гаража");
    }
}




void User_main_page::on_back_button_clicked()
{
    emit close_windows();
}

void User_main_page::on_account_button_clicked()
{
emit account_user();
}

void User_main_page::on_status_button_clicked()
{
    if(this->ui->status_button->text()=="Заехать в гараж"){
        this->ui->status_button->setText("Выехать из гаража");
    }
    else {
        this->ui->status_button->setText("Заехать в гараж");
    }
    emit change_status_avto();
}
