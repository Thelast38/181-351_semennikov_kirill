#-------------------------------------------------
#
# Project created by QtCreator 2017-06-15T10:29:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jsonclient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    register.cpp \
    admin__main_page.cpp \
    user_main_page.cpp \
    personal_account.cpp \
    secur_main_page.cpp \
    list_of_peple.cpp

HEADERS += \
        mainwindow.h \
    register.h \
    admin__main_page.h \
    user_main_page.h \
    personal_account.h \
    secur_main_page.h \
    list_of_peple.h

FORMS += \
        mainwindow.ui \
    register.ui \
    admin__main_page.ui \
    user_main_page.ui \
    personal_account.ui \
    secur_main_page.ui \
    list_of_peple.ui
