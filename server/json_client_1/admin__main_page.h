#ifndef ADMIN__MAIN_PAGE_H
#define ADMIN__MAIN_PAGE_H

#include <QWidget>

namespace Ui {
class admin__Main_page;
}

class admin__Main_page : public QWidget
{
    Q_OBJECT

public:
    explicit admin__Main_page(QWidget *parent = nullptr);
    ~admin__Main_page();
signals:
    void close_windows();
    void open_list_of_people();
private slots:
    void on_listPeople_button_clicked();

    void on_Guard_button_clicked();

    void on_back_Button_clicked();

private:
    Ui::admin__Main_page *ui;
};

#endif // ADMIN__MAIN_PAGE_H
