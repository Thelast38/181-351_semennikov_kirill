#ifndef PERSONAL_ACCOUNT_H
#define PERSONAL_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Personal_account;
}

class Personal_account : public QWidget
{
    Q_OBJECT

public:
    void set_data(QString login,QString number_passport,QString number_car,QString model_car);
    explicit Personal_account(QWidget *parent = nullptr);
    ~Personal_account();
signals:
    void close_windows();
private slots:
    void on_back_button_clicked();

    void on_change_button_clicked();

private:
    Ui::Personal_account *ui;
};

#endif // PERSONAL_ACCOUNT_H
