#include "admin__main_page.h"
#include "ui_admin__main_page.h"

admin__Main_page::admin__Main_page(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::admin__Main_page)
{
    ui->setupUi(this);
}

admin__Main_page::~admin__Main_page()
{
    delete ui;
}

void admin__Main_page::on_listPeople_button_clicked()
{
emit open_list_of_people();
}

void admin__Main_page::on_Guard_button_clicked()
{

}

void admin__Main_page::on_back_Button_clicked()
{
emit close_windows();
}
