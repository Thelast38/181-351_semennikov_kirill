#ifndef REGISTER_H
#define REGISTER_H

#include <QWidget>

namespace Ui {
class Register;
}

class Register : public QWidget
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = nullptr);
    bool check_password(QString password);
    bool check_passport(QString passport);
    bool check_login(QString login);
    bool check_numberCar(QString numberCar);
    bool check_numberCar1(QString numberCar);
    ~Register();
signals:
    void close_windows();
    void new_Register(QString login,QString password,QString passport,QString numberCar,QString modelCar);
private slots:
    void on_push_back_clicked();

    void on_push_register_clicked();

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
