#ifndef SECUR_MAIN_PAGE_H
#define SECUR_MAIN_PAGE_H

#include <QWidget>

namespace Ui {
class secur_main_page;
}

class secur_main_page : public QWidget
{
    Q_OBJECT

public:
    explicit secur_main_page(QWidget *parent = nullptr);
    ~secur_main_page();

private:
    Ui::secur_main_page *ui;
};

#endif // SECUR_MAIN_PAGE_H
