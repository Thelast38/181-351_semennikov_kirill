#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include<string>
#include<user_info_db.h>
#include<user_monitoring.h>
#include <windows.h>
class MyTcpServer : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpServer(QObject *parent = 0);

public slots:
    void slotNewConnection();
    void slotServerRead();
    bool register11(QString code);
    QString login10(QString code);
    void slotClientDisconnected();
    void processing(QString code);
    void change_status_avto(QString login);
    QString personal_account20(QString login);
    QString QByteArray_to_QString(QByteArray templ);
     QByteArray QString_to_QByteArray(QString templ);
     void lilst_peple22(QString key,QString number);

private:
    QString login_data="login_db.txt";
    QString monitoring_data="monitoring_db.txt";
    //user_info_db info_db;
   //user_monitoring monitor_db;
    QTcpServer * mTcpServer;
    QTcpSocket * mTcpSocket;
};

#endif // MYTCPSERVER_H
