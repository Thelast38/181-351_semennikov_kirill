QT += core network
QT -= gui

TARGET = EchoServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    mytcpserver.cpp \
    user_info_db.cpp \
    user_monitoring.cpp

HEADERS += \
    mytcpserver.h \
    user_info_db.h \
    user_monitoring.h

