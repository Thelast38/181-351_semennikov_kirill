#include "mytcpserver.h"
#include <QDebug>
#include <QCoreApplication>
#include<QTextCodec>
#include<string>

MyTcpServer::MyTcpServer(QObject *parent) : QObject(parent)
{
    mTcpServer = new QTcpServer(this);

    connect(mTcpServer, &QTcpServer::newConnection, this, &MyTcpServer::slotNewConnection);

    if(!mTcpServer->listen(QHostAddress::Any, 6000)){
        qDebug() << "server is not started";
    } else {
        qDebug() << "server is started";
    }

}

void MyTcpServer::slotNewConnection()
{
    mTcpSocket = mTcpServer->nextPendingConnection();

    mTcpSocket->write("Hello, World!!! I am echo server!\r\n");
    qDebug()<<"new user";
    connect(mTcpSocket, &QTcpSocket::readyRead, this, &MyTcpServer::slotServerRead);
    connect(mTcpSocket, &QTcpSocket::disconnected, this, &MyTcpServer::slotClientDisconnected);
}

void MyTcpServer::slotServerRead()
{
    while(mTcpSocket->bytesAvailable()>0)
    {
        QByteArray array = mTcpSocket->readAll();
        qDebug()<<array;
        QString s_data;
        s_data.fromUtf8(array);
        std::string buffer=QByteArray_to_QString(array).toStdString();
        processing(QString::fromStdString(buffer));
        //mTcpSocket->write(array);
    }
}

bool MyTcpServer::register11(QString code)
{
    user_info_db db(login_data);
    QString login;
    QString password;
    QString passport;
    QString numberCar;
    QString modelCar;
    QString bfure="00";
    int i=10;
    for (i;i<code.length();i++) {
        if(code[i]=="$"){
            break;
        }
        login+=code[i];
    }
     i+=12;
     for (i;i<code.length();i++) {
         if(code[i]=="$"){
             break;
         }
         password+=code[i];
     }
     i+=12;
     for (i;i<code.length();i++) {
         if(code[i]=="$"){
             break;
         }
         passport+=code[i];
     }
     i+=13;
     for (i;i<code.length();i++) {
         if(code[i]=="$"){
             break;
         }
         numberCar+=code[i];
     }
     i+=12;
     for (i;i<code.length();i++) {
         if(code[i]=="$"){
             break;
         }
         modelCar+=code[i];
     }
    if(db.return_line("login",login)=="error_010"){
        db.setTempl(login,password,passport,numberCar,modelCar);
        db.add_newData();
        return true;
    }
    return false;

}

QString MyTcpServer::login10(QString code)
{
         int i=10;
         user_info_db db(login_data);
         QString login;
         QString hash_password;
        for (i;i<code.length();i++) {
            if(code[i]=="$"){
                break;
            }
            login+=code[i];
        }
        i+=12;
        for (i;i<code.length();i++) {
            if(code[i]=="$"){
                break;
            }
            hash_password+=code[i];
        }
        if(hash_password==db.return_value("login",login,"hash_password")){
            qDebug()<<db.return_value("login",login,"type_account");
            if("user"==db.return_value("login",login,"type_account")){
                QString buffer=db.return_value("login",login,"car_status");
                 qDebug()<<buffer;
                if(buffer=="in"){
                     return "110u1";
                }

               return "110u0";


            }
            if("admin"==db.return_value("login",login,"type_account")){
                return "110a";
            }
            if("secur"==db.return_value("login",login,"type_account")){
                return "110s";
            }
        }
        else {
            return "010";
        }
}

void MyTcpServer::slotClientDisconnected()
{
    qDebug()<<"user disconnect";
    mTcpSocket->close();
}

void MyTcpServer::processing(QString code)
{
    //qDebug()<<code;
    if(code[0]=="1"&& code[1]=="0"){
        QString buffer=login10(code);
        if(buffer=="110u0"){
            qDebug()<<"110u0";
            mTcpSocket->write("110u0");
        }
         else if(buffer=="110u1"){
            qDebug()<<"110u1";
            mTcpSocket->write("110u1");
        }
        else if (buffer=="110a") {
            qDebug()<<"110a";
            mTcpSocket->write("110a");
        }
        else if (buffer=="110s") {
            qDebug()<<"110s";
            mTcpSocket->write("110s");
        }
        else if (buffer=="010"){
            qDebug()<<"010";
             mTcpSocket->write("010");
        }
    }
    if(code[0]=="1"&& code[1]=="1"){

        if(register11(code)){
            qDebug()<<"111";
           mTcpSocket->write("111") ;
        }
        else {
            mTcpSocket->write("011") ;
        };

    }
    if(code[0]=="2"&& code[1]=="0"){

       std::string buffer=code.toStdString();
       buffer=buffer.substr(3,code.length()-4);
       mTcpSocket->write(QString_to_QByteArray(personal_account20(QString::fromStdString(buffer))));
    }
    if(code[0]=="2"&& code[1]=="1"){

       std::string buffer=code.toStdString();
       buffer=buffer.substr(3,code.length()-4);
       change_status_avto(QString::fromStdString(buffer));
    }
    if(code[0]=="2"&& code[1]=="2"){
       lilst_peple22("login","0");
       emit
        Sleep(20000);
       lilst_peple22("passport","1");
        Sleep(20000);
       lilst_peple22("car_number","2");
        Sleep(20000);
       lilst_peple22("car_model","3");
        Sleep(20000);
       lilst_peple22("car_status","4");
        Sleep(20000);

    }

}

void MyTcpServer::change_status_avto(QString login)
{
    user_monitoring db_mon(monitoring_data);
    user_info_db db(login_data);
    if(db.return_value("login",login,"car_status")=="in"){
         db.change_line("login",login,"car_status","out");
         db_mon.setTempl(login,0);
         db_mon.delete_line("login","admin");
         db_mon.add_newData();

    }
    else {
        db.change_line("login",login,"car_status","in");
        db_mon.setTempl(login,1);
        db_mon.delete_line("login","admin");
        db_mon.add_newData();
    }
}

QString MyTcpServer::personal_account20(QString login)
{
    qDebug()<<login;
    user_info_db db(login_data);
    QString buffer;
    buffer+="200$";
    buffer+=db.return_value("login",login,"passport")+"$";
    buffer+=db.return_value("login",login,"car_number")+"$";
    buffer+=db.return_value("login",login,"car_model");
    return buffer;
}

QString MyTcpServer::QByteArray_to_QString(QByteArray templ)
{
    const char *cstr_buffer=templ.data();
    QString str = QString::fromUtf8(cstr_buffer);
    return str;
}

QByteArray MyTcpServer::QString_to_QByteArray(QString templ)
{
    std::string String_buffer=templ.toStdString();
    QByteArray QByteArray_buffer;
    const char *cstr_buffer = String_buffer.c_str();
    QByteArray_buffer.append(cstr_buffer);
    return QByteArray_buffer;
}

void MyTcpServer::lilst_peple22(QString key,QString number)
{
    user_info_db db(login_data);
    QString buf22;
    buf22+=number;
    buf22+="22$";
    buf22+=db.return_value(key);
    mTcpSocket->write(QString_to_QByteArray(buf22));

}

