#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>

/* Подключаем библиотеку, отвечающую за использование WinAPI
 * Данная библиотека необходима для асинхронной проверки состояния клавиш
 * */
#include <windows.h>

// Наследуемся от QGraphicsItem
class Triangle : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Triangle(QObject *parent = 0);
    Triangle(int widht);
    ~Triangle();

signals:
    void signalToDelete();
public slots:
    void slotGameTimer();// Слот, который отвечает за обработку перемещения треугольника
protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

public:
    qreal angle;    // Угол поворота графического объекта

};


#endif // TRIANGLE_H
