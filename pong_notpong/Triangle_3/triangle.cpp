#include"triangle.h"
Triangle::Triangle(QObject *parent) :
    QObject(parent), QGraphicsItem()
{
    angle = 0;     // Задаём угол поворота графического объекта
    setRotation(angle);     // Устанавилваем угол поворота графического объекта
}

Triangle::Triangle(int widht)
{
    angle = 0;
    setRotation(angle);
}

Triangle::~Triangle()
{

}

QRectF Triangle::boundingRect() const
{
    return QRectF(0,0,50,10);   /// Ограничиваем область, в которой лежит треугольник
}

void Triangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
        QPolygon polygon;   /// Используем класс полигона, чтобы отрисовать треугольник
        /// Помещаем координаты точек в полигональную модель
        polygon << QPoint(0,0) << QPoint(50,0) << QPoint(50,10)<< QPoint(0,10);
        painter->setBrush(Qt::red);     /// Устанавливаем кисть, которой будем отрисовывать объект
        painter->drawPolygon(polygon);  /// Рисуем треугольник по полигональной модели
        Q_UNUSED(option);
        Q_UNUSED(widget);
}

void Triangle::slotGameTimer()
{
    /* Поочерёдно выполняем проверку на нажатие клавиш
     * с помощью функции асинхронного получения состояния клавиш,
     * которая предоставляется WinAPI
     * */



    if(GetAsyncKeyState(VK_LEFT)){
         setPos(mapToParent(-10, 0));
    }
    if(GetAsyncKeyState(VK_RIGHT)){
        setPos(mapToParent(10, 0)); // Поворачиваем объект
    }
    if(GetAsyncKeyState(VK_F4)){
        emit signalToDelete(); // Поворачиваем объект
    }
    /* Проверка выхода за границы поля
     * Если объект выходит за заданные границы, то возвращаем его назад
     * */
    if(this->x() - 10 < -260){
        this->setX(-250);       // слева
    }
    if(this->x() + 10 > 210){
        this->setX(200);        // справа
    }

    if(this->y() - 10 < -250){
        this->setY(-240);
    }
    if(this->y() + 10 > 250){
        this->setY(240);        // снизу
    }
}
