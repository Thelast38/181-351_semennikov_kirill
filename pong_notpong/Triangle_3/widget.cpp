#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->resize(600,600);          // Задаем размеры виджета, то есть окна
    this->setFixedSize(600,600);    // Фиксируем размеры виджета

    scene = new QGraphicsScene();   // Инициализируем графическую сцену
    triangle = new Triangle();      // Инициализируем треугольник
    Circle = new circle();
    for (int i=0;i<5;i++) {
        blocks= new Block();
        scene->addItem(blocks);
        blocks->setPos(-250+i*100,-250);
        bloki.append(blocks);
    }
    for (int i=0;i<5;i++) {
        blocks= new Block();
        scene->addItem(blocks);
        blocks->setPos(-250+i*100,-230);
        bloki.append(blocks);
    }
    ui->graphicsView->setScene(scene);  // Устанавливаем графическую сцену в graphicsView
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);    // Устанавливаем сглаживание
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff); // Отключаем скроллбар по вертикали
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff); // Отключаем скроллбар по горизонтали

    scene->setSceneRect(-250,-250,500,500); // Устанавливаем область графической сцены

    // Добавляем вертикальную линию через центр

    /* Дополнительно нарисуем органичение территории в графической сцене */
       scene->addLine(-250,-250, 250,-250, QPen(Qt::black));
       scene->addLine(-250, 250, 250, 250, QPen(Qt::black));
       scene->addLine(-250,-250,-250, 250, QPen(Qt::black));
       scene->addLine( 250,-250, 250, 250, QPen(Qt::black));
    //scene->addItem(blocks);
    scene->addItem(triangle); // Добавляем на сцену треугольник
    scene->addItem(Circle);
    Circle->setPos(-5,210);
    triangle->setPos(-25,250);      // Устанавливаем треугольник в центр сцены
    //blocks->setPos(-250,-250);
    /* Инициализируем таймер и вызываем слот обработки сигнала таймера
     * у Треугольника 20 раз в секунду.
     * Управляя скоростью отсчётов, соответственно управляем скоростью
     * изменения состояния графической сцены
     * */
    timer = new QTimer();
    connect(timer, &QTimer::timeout, triangle, &Triangle::slotGameTimer);
    connect(timer, &QTimer::timeout, Circle, &circle::slotGameTimer);
    connect(timer, &QTimer::timeout, this, &Widget::BlockOb);
    connect(Circle, &circle::signalCheckItem, this, &Widget::slotDeleteBlock);
    connect(triangle,&Triangle::signalToDelete, this, &Widget::slotSetygol);
    timer->start(1000 / 50);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::BlockOb()
{
    if(Circle->y()>235){
       if(Circle->x()>triangle->x()-10 && Circle->x()<triangle->x()+60){
            Circle->znak*=-1;
            Circle->angle=360-Circle->angle;
            Circle->setRotation(Circle->angle);
        }
    }
    if(Circle->y()>248){
        QMessageBox msgBox;
        msgBox.setText("You lose");
        msgBox.exec();
        this->close();
    }




}

void Widget::slotDeleteBlock(QGraphicsItem *item)
{
    /* Получив сигнал от Мухи
     * Перебираем весь список яблок и удаляем найденное яблоко
     * */
    foreach (QGraphicsItem *apple, bloki) {
        if(apple == item){
           apple->setY(500);   // Удаляем со сцены
            bloki.removeOne(item);     // Удаляем из списка
            delete apple;               // Вообще удаляем
            if(bloki.empty()==true){
                QMessageBox msgBox;
                msgBox.setText("You win");
                msgBox.exec();
                this->close();
            }
        }
    }
}

void Widget::slotSetygol()
{
    foreach (QGraphicsItem *apple, bloki) {
        bloki.removeOne(apple);
        delete apple;
        if(bloki.empty()==true){
            QMessageBox msgBox;
            msgBox.setText("You win");
            msgBox.exec();
            this->close();
        }
        break;
    }
}


