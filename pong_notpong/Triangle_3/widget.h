#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QDebug>
#include<QTimer>
#include <QShortcut>
#include <QGraphicsScene>
#include<iostream>
#include <blocks.h>
#include <triangle.h>
#include<circle.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
    void BlockOb();
    void slotDeleteBlock(QGraphicsItem *item);
    void slotSetygol();
private:
    Ui::Widget      *ui;
    QGraphicsScene  *scene;     // Объявляем графическую сцену
    Triangle        *triangle;
    Block *blocks;
    circle *Circle;// и треугольник
    QTimer *timer;
     QList<QGraphicsItem *> bloki;
};

#endif // WIDGET_H
