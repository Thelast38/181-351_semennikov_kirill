#ifndef BLOCK_H
#define BLOCK_H
#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>

/* Подключаем библиотеку, отвечающую за использование WinAPI
 * Данная библиотека необходима для асинхронной проверки состояния клавиш
 * */
#include <windows.h>

// Наследуемся от QGraphicsItem
class Block : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Block(QObject *parent = 0);
    Block(int widht);
    ~Block();

signals:

public slots:
    void slotGameTimer();// Слот, который отвечает за обработку перемещения треугольника
protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    qreal angle;    // Угол поворота графического объекта

};

#endif // BLOCK_H
