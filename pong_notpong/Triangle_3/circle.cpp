#include "circle.h"
#include<QTimer>
#include<iostream>
#include<ctime>

circle::circle(QObject *parent):
    QObject(parent), QGraphicsItem()
{
angle=45;
setRotation(angle);
}

circle::~circle()
{

}

void circle::slotGameTimer()
{
    y1+=0.001;
    srand(time(nullptr));
   int a = std::rand()%10;
    if(this->y()<0){
 QList<QGraphicsItem *> foundItems = scene()->items(QPolygonF()
                                                             << mapToScene(0, 0)
                                                             << mapToScene(0, 10)
                                                             << mapToScene(10, 10)
                                                    << mapToScene(10, 0));
       /* После чего проверяем все элементы.
        * Один из них будет сама Муха - с ней ничего не делаем.
        * А с остальными высылаем сигнал в ядро игры
        * */
       foreach (QGraphicsItem *item, foundItems) {
           if (item == this)
               continue;
           emit signalCheckItem(item);
           znak*=-1;
           angle=360-angle+a;
           setRotation(angle);
           break;
       }
       }
setPos(mapToParent(0, y1*(znak)));
if(this->x()>240){
    znak*=-1;
    angle=180-angle+a;
    setRotation(angle);
            setX(230);
}
if(this->x()<-250){
    znak*=-1;
    angle=180-angle+a;
    setRotation(angle);
    setX(-240);
}
if(this->y()<-240){
    znak*=-1;
    angle=360-angle+a;
    emit signalCheck();
    setRotation(angle);
    setY(-230);
}


}

QRectF circle::boundingRect() const
{
    return QRectF(0,0,10,10);
}

void circle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rectangle(0,0,10,10);

    painter->setBrush(Qt::blue);
    painter->drawEllipse(rectangle);
    Q_UNUSED(option);
    Q_UNUSED(widget);

}
