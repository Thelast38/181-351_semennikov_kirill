#ifndef CIRCLE_H
#define CIRCLE_H
#include"triangle.h"
#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include<QMessageBox>

class circle: public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit circle(QObject *parent = 0);
    ~circle();
public slots:
    void slotGameTimer();
signals:
    void signalCheckItem(QGraphicsItem *item);
    void signalCheck();
protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
public:
    double y1=8;
    int znak=-1;
    qreal angle;
};

#endif // CIRCLE_H
