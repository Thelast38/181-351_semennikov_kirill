#include"blocks.h"
Block::Block(QObject *parent) :
    QObject(parent), QGraphicsItem()
{
    angle = 0;     // Задаём угол поворота графического объекта
    setRotation(angle);     // Устанавилваем угол поворота графического объекта
}

Block::Block(int widht)
{
    angle = 0;
    setRotation(angle);
}

Block::~Block()
{

}

QRectF Block::boundingRect() const
{
    return QRectF(0,0,100,20);   /// Ограничиваем область, в которой лежит треугольник
}

void Block::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
        QPolygon polygon;   /// Используем класс полигона, чтобы отрисовать треугольник
        /// Помещаем координаты точек в полигональную модель
        polygon << QPoint(0,0) << QPoint(100,0) << QPoint(100,20)<< QPoint(0,20);
        painter->setBrush(Qt::green);     /// Устанавливаем кисть, которой будем отрисовывать объект
        painter->drawPolygon(polygon);  /// Рисуем треугольник по полигональной модели
        Q_UNUSED(option);
        Q_UNUSED(widget);
}

void Block::slotGameTimer()
{
    /* Поочерёдно выполняем проверку на нажатие клавиш
     * с помощью функции асинхронного получения состояния клавиш,
     * которая предоставляется WinAPI
     * */



    if(GetAsyncKeyState(VK_LEFT)){
         setPos(mapToParent(-10, 0));
    }
    if(GetAsyncKeyState(VK_RIGHT)){
        setPos(mapToParent(10, 0)); // Поворачиваем объект
    }
   /* if(GetAsyncKeyState(VK_UP)){
        setPos(mapToParent(0, -5));      Продвигаем объект на 5 пискселей вперёд
                                          перетранслировав их в координатную систему
                                          графической сцены

    }*/
   /*  if(GetAsyncKeyState(VK_DOWN)){
        setPos(mapToParent(0, 5));      /* Продвигаем объект на 5 пискселей назад
                                         * перетранслировав их в координатную систему
                                         * графической сцены
                                         * */
//    }

    /* Проверка выхода за границы поля
     * Если объект выходит за заданные границы, то возвращаем его назад
     * */
    if(this->x()< -250){
        this->setX(-250);       // слева
    }
    if(this->x() + 10 > 210){
        this->setX(200);        // справа
    }

    if(this->y() - 10 < -250){
        this->setY(-240);       // сверху
    }
    if(this->y() + 10 > 250){
        this->setY(240);        // снизу
    }
}
